module.exports = class CommandExecutor {
  map

  constructor(map) {
    this.map = map;
  }

  async executeCommand (args) {
    if (args['_'].length === 0 && !args.help && (!args.version && !args.v)) {
      throw new Error(`Invalid command. See obsidian-trx-mapper --help.`)
    }

    if (args['_'][0] === 'map') {
      await this.map.handleCommand(args);
    } else if (args.help) {
      console.log(
`obsidian-trx-mapper COMMAND
    [--help]
    [--version, -v]
    [--verbose]
    
Valid commands:
    map (see obsidian-trx-mapper map --help)`);
    } else if (args.version || args.v) {
      var pjson = require('../package.json');
      console.log(
`obsidian-trx-mapper v${pjson.version}`
      );
    } else {
      throw new Error(`Unable to find a module to process this command!`);
    }
  }
}


