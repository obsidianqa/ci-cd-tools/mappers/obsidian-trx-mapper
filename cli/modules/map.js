class Map {
  xml2js
  fs
  moment
  requiredArgs = [
    'output'
  ]

  constructor(xml2js, fs, moment) {
    this.xml2js = xml2js;
    this.fs = fs;
    this.moment = moment;
  }

  async handleCommand(args) {
    if (args['_'].length < 2) {
      throw new Error(`You must provide a file to map! See obsidian-trx-mapper map --help.`)
    }

    for (let i = 0; i < this.requiredArgs.length; i++) {
      if(!args[this.requiredArgs[i]]) {
        throw new Error(`Required argument ${this.requiredArgs[i]} not supplied. See obsidian-trx-mapper map --help.`)
      }
    }

    try {
      const inputFilename = args['_'][1];
      const inputFileContent = await this.fs.readFile(inputFilename);
      const result = await this.xml2js.parseStringPromise(inputFileContent);

      let testRun = {
        startTimeUtc: result.TestRun.Times[0]['$'].start,
        endTimeUtc: result.TestRun.Times[0]['$'].finish
      };

      let executions = [];
      for (let i = 0; i < result.TestRun.Results[0].UnitTestResult.length; i++) {
        let testResult = result.TestRun.Results[0].UnitTestResult[i];

        executions.push({
          message: testResult.Output && testResult.Output[0] && testResult.Output[0].ErrorInfo[0].Message[0].replace('\n', ',').replace(/\s+/g,' ').trim(),
          succeeded: testResult['$'].outcome === 'Passed',
          testName: testResult['$'].testName,
          stackTrace: testResult.Output && testResult.Output[0] && testResult.Output[0].ErrorInfo[0].StackTrace[0].replace('\n', ',').replace(/\s+/g,' ').trim(),
          duration: this.moment(testResult['$'].duration, 'HH:mm:ss.SSSSSSS').millisecond(),
        });
      }

      testRun.success = executions.every(exec => exec.succeeded);
      testRun.passes = executions.filter(exec => exec.succeeded).length;
      testRun.failures = executions.filter(exec => !exec.succeeded).length;
      testRun.executions = executions;

      await this.fs.writeFile(args.output, JSON.stringify(testRun));
    } catch(err) {
      if (args.verbose) {
        console.error(err);
      }

      throw new Error(`Encountered an error while reading and mapping your input TRX. ${err}`);
    }
  }
}

module.exports = Map;